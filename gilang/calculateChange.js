calculate = (total, pay) => {
  console.log(
    "===================================================================="
  );
  if (pay < total) {
    return console.log("not enough");
  }

  let change = pay - total;

  const changeOut = change.toString();

  const value = [50000, 20000, 10000, 5000, 2000, 1000, 500, 200, 100];

  const money = [];
  let pos;

  const satuan = {
    50000: 0,
    20000: 0,
    10000: 0,
    5000: 0,
    2000: 0,
    1000: 0,
    500: 0,
    200: 0,
    100: 0,
  };

  while (true) {
    value.map((x) => {
      if (change >= x) {
        change = change - x;
        satuan[x] += 1;
        pos = x;
        money.push(x);
      }
    });
    if (change <= pos) {
      break;
    }
  }

  const last = parseInt(changeOut.substring(0, changeOut.length - 2) + "00");
  console.log("your change is : ", last);

  Object.entries(satuan).forEach((r) => {
    if (parseInt(r[1]) >= 1) {
      if (parseInt(r[0]) < 1000) {
        console.log(r[1], " koin ", r[0], "rupiah");
      } else {
        console.log(r[1], "lembar", r[0], "rupiah");
      }
    }
  });
};
// total pay
calculate(700649, 800000);
calculate(575650, 580000);
calculate(657650, 600000);
// calculate(650, 600000);
