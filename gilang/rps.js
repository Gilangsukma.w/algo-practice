rps = (input) => {
  const generate = Math.floor(Math.random() * 2);
  let e;

  const validate = ["rock", "scissor", "paper"];

  if (!validate.includes(input)) {
    return "wrong input";
  }

  if (generate === 0) {
    e = "rock";
  }

  if (generate === 1) {
    e = "scissor";
  }

  if (generate === 2) {
    e = "paper";
  }

  console.log("=====================");
  if (input === "rock" && e === "rock") {
    return "draw";
  }

  if (input === "rock" && e === "scissor") {
    return "you lose";
  }

  if (input === "rock" && e === "paper") {
    return "you win";
  }

  //==============================================

  if (input === "rock" && e === "paper") {
    return "you lose";
  }

  if (input === "paper" && e === "rock") {
    return "you win";
  }

  if (input === "paper" && e === "scissor") {
    return "you lose";
  }

  //==============================================

  if (input === "scissor" && e === "rock") {
    return "you lose";
  }

  if (input === "scissor" && e === "paper") {
    return "you win";
  }

  if (input === "scissor" && e === "scissor") {
    return "you draw";
  }
};

console.log(rps("rock"));
console.log(rps("paper"));
console.log(rps("scissor"));
console.log(rps("nuclear"));
console.log(rps(1));
